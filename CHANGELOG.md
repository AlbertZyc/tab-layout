## [v1.0.14] 2024-08-07
- 修复：删除错误的日志
- 
## [v1.0.13] 2024-08-06
- 修复：初始化可能会导致的崩溃

## [v1.0.12] 2024-08-01
- 修复：Swiper高度设置为layoutWeight(1)

## [v1.0.11] 2024-07-23

- 版本：本次改动API12以下无法运行
- 支持：点击tab切换pageContent时page会有动画效果
- 支持：使用defaultIndex来创建默认页面
- 支持：设置背景色和圆角设置，新增borderXXXX、backgroundXXX
- 弃用：tabAlignment现在被弃用了，使用alignment
- 弃用：tabPadding现在被弃用了，使用padding
- 弃用：smartToIndex，使用使用pageHorSwiperController来控制
- 变更：tabContainerWidth现在默认为auto
- 变更：启用openSwiperAnimation
- 变更：在动画期间滑动和点击将有可能被拦截
- 修复：tab样式中文字在选中时会换行（API计算有差异，现在会默认+1px）
- 修复: 设置tabs等分，不生效设置tabs等分，不生效
- 问题：defaultIndex不在当前屏幕暂时不会触发滚动和绘制
- 问题：自定义Tab更新问题
- 预告：Tab和Content分离，使用Controller衔接

## [v1.0.10] 2024-06-11

- 修复：tabContainerMargin 和 tabContainerPadding在可滑动时，指示器表现异常

## [v1.0.9] 2024-05-28

- 修改：cachedCount去除
- 新增：数据源可动态添加

## [v1.0.8] 2024-05-27

- 修改：默认指示器使用Stack
- 修复：指示器有一条黑线的BUG
- 修改：容器默认不设置layoutWeight
-

## [v1.0.7] 2024-05-21

- 规范：整理方法
- 支持：支持均分Tab容器显示Tab
- 新增：tabAverageEnable - def：false （必须传入tabContainerWidth）
- 新增：tabContainerWidth - def：auto
- 修复：Indicator的Padding和Margin显示
- 修复：Indicator的宽度将受到warpTab，padding，margin的影响
- 修改：默认tabPadding修改为0
- 弃用：暂时弃用gapContinuousAnimation属性

## [v1.0.6] 2024-05-21

- 修复：只使用Tab时，onTabSelect不回调
- 修复：tabContainerPadding作用位置错误
- 支持：新增tabContainerMargin

## [v1.0.5] 2024-05-20

- 修复：tabContainerPadding没有被正确计算的问题
- 支持：自定义Indicator 可以使用 切换动画 ，而不是滑动动画了
- 支持：提供更多的用例

## [v1.0.4] 2024-05-19

- 修复：滚动时指示器位置跳动的BUG

## [v1.0.3] 2024-05-01

- 修复:屏幕外TabItem获取异常的问题

## [v1.0.2] 2024-04-26

- 支持：超过内容长度时，Tab可滚动
- 支持：超过内容长度时，切换Tab，指示器将自动滚动尝试滚动到中央（仿拼多多）

## [v1.0.1] 2024-04-25

- 更新文档

## [v1.0.0] 2024-04-23

- 新增指示器Block、Dot、Line样式
- 新增指示器渐变色、圆角、宽度、高度、位置
- 新增选中/非选中字体、大小、颜色等样式
- 新增Tab排列方式，左、中、右
- 新增滑动动画
- 新增smartToIndex可以主动控制指示器滑动
- 新增pageHorSwiperController，可以主动控制Page滑动
